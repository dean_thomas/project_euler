﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace ProjectEuler
{
    class Problem25
    {
        static Tuple<BigInteger, BigInteger> fastFib(BigInteger k, BigInteger i)
        {
            //  using k = F(k) and i = F(k+1)
            //  and x = F(2k) and y = F(2k+1)
            BigInteger x = k * ((2 * i) - k);
            BigInteger y = (i * i) + (k * k);

            return new Tuple<BigInteger, BigInteger>(x ,y);
        }

        static void Main(string[] args)
        {
            Dictionary<uint, BigInteger> lookUp = new Dictionary<uint, BigInteger>();

            lookUp.Add(1, 1);
            lookUp.Add(2, 1);

            uint k = 1;
            do
            {
                var fibs = fastFib(lookUp[k], lookUp[k+1]);
                
                lookUp[2 * k] = fibs.Item1;
                lookUp[2 * k + 1] = fibs.Item2;

                ++k;
            } while (k < 10000);

            var i = 1u;
            do
            {
                Console.Out.WriteLine(i + ":" + lookUp[i].ToString().Length);
                ++i;
            } while (lookUp[i].ToString().Length <= 1000);
        }
    }
}
