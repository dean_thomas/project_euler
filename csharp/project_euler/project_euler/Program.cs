﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using project_euler.p0001_p0010;

namespace project_euler
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine(new Problem1());
            Console.WriteLine(new Problem2());
            Console.WriteLine(new Problem3());
            Console.WriteLine(new Problem4());
            Console.WriteLine(new Problem5());
            Console.WriteLine(new Problem6());
            Console.WriteLine(new Problem7());
            Console.WriteLine(new Problem8());
            Console.WriteLine(new Problem9());

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
