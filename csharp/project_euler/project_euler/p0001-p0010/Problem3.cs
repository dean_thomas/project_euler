﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem3 : Problem
    {
        public override uint ID
        {
            get
            {
                return 3;
            }
        }

        public override BigInteger calculate()
        {
            return Algorithms.PrimeFactors(600851475143).Max();
        }
    }
}
