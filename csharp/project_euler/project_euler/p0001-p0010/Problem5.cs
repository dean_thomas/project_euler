﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem5 : Problem
    {
        public override uint ID
        {
            get
            {
                return 5;
            }
        }

        public override BigInteger calculate()
        {
            return Algorithms.lcm(PositiveIntegers.Sequence().Take(20).ToArray());
        }
    }
}
