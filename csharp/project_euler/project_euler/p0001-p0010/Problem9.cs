﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem9 : Problem
    {
        public override uint ID
        {
            get
            {
                return 9;
            }
        }

        public override BigInteger calculate()
        {
            //  Builds a enumerable of potential triplets for checking
            var triplets = from a in Enumerable.Range(1, 1000)
                           from b in Enumerable.Range(a, (1000 - a) / 2 - a)
                           let c = 1000 - a - b
                           select new { a, b, c };

            //  Selects only those triplets where a^2 + b^2 = c^2
            var triple = (from x in triplets
                         where x.a * x.a + x.b * x.b == x.c * x.c
                         select x).First();

            //  Compute the product
            return triple.a * triple.b * triple.c;
        }
    }
}
