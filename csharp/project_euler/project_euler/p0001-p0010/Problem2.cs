﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem2 : Problem
    {
        public override uint ID
        {
            get
            {
                return 2;
            }
        }

        public override BigInteger calculate()
        {
            var query = Fibionacci.Sequence().TakeWhile(x => x < 4000000).Where(x => (x % 2 == 0));
            return query.Aggregate(BigInteger.Add);
        }
    }
}
