﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem4 : Problem
    {
        public override uint ID
        {
            get
            {
                return 4;
            }
        }

        private IEnumerable<BigInteger> nums(BigInteger min, BigInteger max)
        {
            var xs = For.Sequence(min, max);
            var ys = For.Sequence(min, max);

            foreach (var x in xs)
            {
                foreach (var y in ys)
                {
                    yield return x * y;
                }
            }
        }

        public override BigInteger calculate()
        {
            return nums(100, 1000).Where(Algorithms.isPalindrome).OrderBy(x => x).Last();
        }
    }
}
