﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace project_euler.p0001_p0010
{
    class Problem1 : Problem
    {
        public override uint ID
        {
            get
            {
                return 1;
            }
        }

        public override BigInteger calculate()
        {
            var query = PositiveIntegers.Sequence().TakeWhile(x => x < 1000).Where(x => (x % 3 == 0) || (x % 5 == 0));
            return query.Aggregate(BigInteger.Add);
        }
    }
}
