﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem7 : Problem
    {
        public override uint ID
        {
            get
            {
                return 7;
            }
        }

        public override BigInteger calculate()
        {
           var primes = Primes.Sequence().Take(10001);
           return primes.Last();
        }
    }
}
