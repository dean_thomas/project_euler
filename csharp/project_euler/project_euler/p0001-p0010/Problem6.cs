﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace project_euler.p0001_p0010
{
    class Problem6 : Problem
    {
        public override uint ID
        {
            get
            {
                return 6;
            }
        }

        public override BigInteger calculate()
        {
            const int LIMIT = 100;

            var sumOfSquares = PositiveIntegers.Sequence().Take(LIMIT).Select(x => x*x).Aggregate(BigInteger.Add);
            var squareOfSums = BigInteger.Pow(PositiveIntegers.Sequence().Take(LIMIT).Aggregate(BigInteger.Add), 2);
            return squareOfSums - sumOfSquares;
        }
    }
}
