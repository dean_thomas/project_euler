﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
public static class Enumerable
{
    public static IEnumerable<BigInteger> Range(BigInteger from, BigInteger count)
    {
        for (BigInteger i = from; i < from + count; i++) yield return i;
    }
}

