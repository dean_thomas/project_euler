﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace project_euler
{
    abstract class Problem
    {
        public abstract uint ID { get; }
        public abstract BigInteger calculate();

        public override string ToString()
        {
            var stopWatch = System.Diagnostics.Stopwatch.StartNew();
            var result = calculate();
            stopWatch.Stop();

            return String.Format("Project Euler problem {0} answer is: {1}.  Computation took: {2}ms.", ID, result, stopWatch.ElapsedMilliseconds);
        }
    }
}
