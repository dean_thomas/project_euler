﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;

public static class Primes
{
    public static IEnumerable<BigInteger> Sequence()
    {
        return PositiveIntegers.Sequence().Where(Algorithms.isPrime);
    }
}

public static class For
{
    public static IEnumerable<BigInteger> Sequence(BigInteger from, BigInteger to)
    {
        for (var i = from; i < to; i++) yield return i;
    }
}

public static class PositiveIntegers
{
    public static IEnumerable<BigInteger> Sequence()
    {
        //  Initialise 
        BigInteger i = 1;

        //  Loop forever
        while (true)
        {
            //  Return next
            yield return i;

            //  Increment for next call
            ++i;
        }
    }
}

public static class Fibionacci
{
    public static IEnumerable<BigInteger> Sequence()
    {
        //  Initialise (where i = f_0; j = f_1)
        BigInteger i = 1;
        BigInteger j = 2;

        //  First two calls return f_0 and f_1
        yield return i;
        yield return j;

        //  Loop forever
        while (true)
        {
            //  Compute next
            var next = i + j;

            //  Return next
            yield return next;

            //  Increment for next call
            i = j;
            j = next;
        }
    }
}
