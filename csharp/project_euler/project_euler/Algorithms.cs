﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

public static class Algorithms
{
    public static bool isPalindrome(BigInteger number)
    {
        return number.ToString().Equals(new String(number.ToString().Reverse().ToArray()));
    }
    public static bool isPrime(BigInteger number)
    {
        if (number == 1) return false;
        if (number == 2) return true;
        
        //var factorString = "";
        //foreach (var n in Factors(number)) factorString += "\t" + n;

        //Console.WriteLine("Factors of {0} are {1}", number, factorString);
        
        return Factors(number).SequenceEqual(new List<BigInteger> { 1, number });
    }

    /// <summary>
    /// Computes the greatest common divisor using Euclids algorithm
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static BigInteger gcd(BigInteger a, BigInteger b)
    {
        var x = BigInteger.Min(a, b);
        var y = BigInteger.Max(a, b);

        while (y != 0)
        {
            var z = x % y;
            x = y;

            if (z == 0)
                return x;
            else
                y = z;
        }
        return 0;
    }

    /// <summary>
    /// Computes the least common multiple of 2 numbers using
    /// the greatest common denominator (GCD)
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static BigInteger lcm(BigInteger a, BigInteger b)
    {
        return a * b / gcd(a, b);
    }

    /// <summary>
    /// Computes the least common multiple (LCM) of multiple numbers
    /// </summary>
    /// <param name="parameters">An array of numbers to find the LCM from</param>
    /// <returns></returns>
    public static BigInteger lcm(params BigInteger[] parameters)
    {
        switch (parameters.Length)
        {
            case 0: throw new IndexOutOfRangeException();
            case 1: throw new IndexOutOfRangeException();
            case 2: return lcm(parameters[0], parameters[1]);
            default:
                BigInteger[] p = new BigInteger[parameters.Length-1];
                Array.Copy(parameters, p, parameters.Length - 1);
                return lcm(parameters[parameters.Length-1], lcm(p));
        }
    }

    public static IEnumerable<BigInteger> PrimeFactors(BigInteger number)
    {
        return Factors(number).Where(isPrime);
    }

    public static IEnumerable<BigInteger> Factors(BigInteger number)
    {
        //  All numbers (n) have a factor of 1
        yield return 1;

        //if (number > 2)
        //if (number % 2 == 0) yield return 2;

        var i = 2;
        var max = new BigInteger(Math.Round(sqrt(number)));
        while (i <= max)
        {
            if (number % i == 0) yield return i;
            i += 1;
        }

        //  All numbers (n) have a factor of n
        yield return number;
    }

    private static double sqrt(BigInteger pd)
    {
        var result = Math.Pow(Math.E, BigInteger.Log(pd) / 2);
        return result;
    }
}
