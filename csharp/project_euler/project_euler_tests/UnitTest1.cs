﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using project_euler.p0001_p0010;

namespace project_euler_tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestProblems0001_0010()
        {
            Assert.AreEqual(new Problem1().calculate(), 233168);
            Assert.AreEqual(new Problem2().calculate(), 4613732);
            Assert.AreEqual(new Problem3().calculate(), 6857);
            Assert.AreEqual(new Problem4().calculate(), 906609);
            Assert.AreEqual(new Problem5().calculate(), 232792560);
            Assert.AreEqual(new Problem6().calculate(), 25164150);
            Assert.AreEqual(new Problem7().calculate(), 104743);
            Assert.AreEqual(new Problem8().calculate(), 23514624000);
            Assert.AreEqual(new Problem9().calculate(), 31875000);
        }
    }
}
