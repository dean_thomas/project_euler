# README #


### What is this repository for? ###

* Implements the [Project Euler](https://projecteuler.net) problems in a range of languages.

### How do I get set up? ###

* For Haskell: [iHaskell](https://github.com/gibiansky/IHaskell)